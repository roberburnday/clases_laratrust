<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//modelos
use app\Models\User;
use app\Models\Role;
use app\Models\Permission;

//utilidades
//use route/rouet/Auth;

class AcessControllerTest extends Controller
{
    //
    private $booleanResultRole; //también podrías usar protected, todo depende de que intenciones tengas
    private $booleanResultAcces;

    /**
     * El construtor se inicia siempre al entrar en cualquier clase
     */
    function __construct() {
        //check roles en el constructor
        $user = Auth::user();
        //checkea que tenga uno de los roles ( || )
        $this->booleanResultRole = $user->hasRole(['superRoot', 'admin']);       
        //checkeas que el usuario tenga los 2 roles (obligatoriamente) ( && )
        $this->booleanResultRole = $user->hasRole(['superRoot', 'admin'], true);   
        //checkeas que tenga acceso a uno de esos 2 permisos ( || )
        $this->booleanResultAcces = $user->isAbleTo(['edit-user', 'create-post']);
        //checkeas que tenga acceso los 2 permisos (obligatoriamente) ( && )
        $this->booleanResultAcces = $user->isAbleTo(['edit-user', 'create-post'], true);
    }

    public function formCrearPost(){
        //cheks simples de role y permiso
        $booleanResultRole = Auth::user()->hasRole('user');
        $booleanResultAcces = Auth::user()->hasPermission('create-post');
        //checks de rol y permiso a la vez
        $booleanCheckRoleAndAcces = $user->ability(['admin', 'user'], ['create-post', 'edit-user']);
    }

    /**
     * Aquí un ejemplo de como usar el hasRole y el hasPermission
     */
    public function requestCrearPost(Request $request){
        $comentarios = null;
        if(Auth::user()->hasRole('admin') || Auth::user()->hasPermission('ver-comentarios')){
            //aquí get all del modelo
            $comentario = ["return", "del", "modelo"];
        }

        //llamada a la vista con el return del modelo
    }

    public function formEditUserPermissions(){
        //filtrado de usuarios por roles
        $users = User::whereRoleIs('admin')->orWhereRoleIs('user')->get();
        $users = User::whereRoleIs(['admin', 'user'])->get();
        //filtrado de usuarios por permiso
        $users = User::wherePermissionIs(['send-mercancia', 'update-mercancia'])->get();
        //filtrado de usuarios que no tienen role o permiso
        $users = User::whereDoesntHaveRole()->get();
        $users = User::whereDoesntHavePermission()->get();
    }
}
