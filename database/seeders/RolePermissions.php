<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

//modelos
use app\Models\User;
use app\Models\Role;
use app\Models\Permission;

//utilidades
use Illuminate\Support\Facades\Hash;

class RolePermissions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Creamos roles
        $owner = Role::create([
            'name' => 'superRoot',
            'display_name' => 'Super Root', // optional
            'description' => 'Acceso a todas las partes de la plataforma', // optional
        ]);
        
        $admin = Role::create([
            'name' => 'admin',
            'display_name' => 'Administrador', // optional
            'description' => 'Usuario administrador con poder sobre los usuarios simples', // optional
        ]);

        $simpleRolUser = Role::create([
            'name' => 'user',
            'display_name' => 'Usuario', // optional
            'description' => 'Usuario con acciones básicas', // optional
        ]);

        //creamos los permissions
        $createPost = Permission::create([
            'name' => 'create-post',
            'display_name' => 'Create Posts', // optional
            'description' => 'Crear post en la plataforma', // optional
        ]);
        
        $createUser = Permission::create([
            'name' => 'create-user',
            'display_name' => 'Create Users', // optional
            'description' => 'Crear nuevos usuarios', // optional
        ]);

        $editUser = Permission::create([
            'name' => 'edit-user',
            'display_name' => 'Edit Users', // optional
            'description' => 'Editar usuarios existentes', // optional
        ]);

        //Asignamos permisos
        $admin->attachPermission($editUser);
        $simpleRolUser->attachPermissions([$createPost, $editUser]);
        $owner->attachPermissions([$createUser, $createPost, $editUser]);

        //creeamos un usuario para el rol superRoot y le asignamos el rol
        $user = User::create([ 
            'name'=>'SuperRootUser', //nombre
            'email'=>'SuperRootUser@portal.com', //email
            'password'=>Hash::make('password-SuperRootUser') //contraseña
            //Hash::make sirve para encriptar cadenas de textos de forma segura
            //en concreto, el auth de laravel, lo usa con las contraseñas
        ]);
        $user->detachRole($owner);
    }
}
