<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]); 
//Auth::routes();

//Ruta accesible públicamente
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//rutas protegidas por sesión (no importa rol o permiso)
Route::get('/rutaAccesible/SoloSiHasHechoLogin', 'AccessControllerTest@requestCrearPost')
->middleware('auth');

//rutas protegidas por rol
Route::group([
    'prefix' => 'nombreGrupoRutas', 
    'middleware' => ['role:admin']
], function() {
    Route::get('/edit', 'AccessControllerTest@requestCrearPost'); //ruta final: /nombreGrupoRutas/edit
    Route::post('/save', 'AccessControllerTest@requestCrearPost'); //ruta final: /nombreGrupoRutas/edit
});

//ruta protegida por permiso
Route::get('/otraRuta','AccessControllerTest@requestCrearPost')
->middleware('permission:edit-post|edit-user');

